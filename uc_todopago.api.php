<?php
/**
 * @file
 * Hooks provided by this module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Acts on commerce data array being loaded.
 *
 * This hook is invoked during order data array loading to send the method sendAuthorizeRequest.
 *
 * @param array $data
 *   An array of order data being loaded.
 *
 * @param $order
 *   The ubercart order object.
 */
function hook_uc_todopago_authorization_request_data_order($order, &$data) {
  $customer_email = $order->primary_email;
  $amount = number_format($order->order_total, 2, '.', '');
  /**
   * Fraud prevention: Generic fields
   */
  $data['CSBTCITY'] = 'El Bolsón';
  $data['CSBTFIRSTNAME'] = 'Pepe';
  $data['CSBTLASTNAME'] = 'Sanchez';
  $data['CSBTPHONENUMBER'] = '1123456789';
  $data['CSBTPOSTALCODE'] = '8430';
  $data['CSBTSTREET1'] = 'Street';

  /**
   * Fraud prevention: line item fields
   */
  $product = reset($order->products);

  $data['CSITPRODUCTCODE'] = substr(trim($product->nid, '#') , 0, 255);
  $data['CSITPRODUCTDESCRIPTION'] = substr(trim($product->title, '#'), 0, 255);
  $data['CSITPRODUCTNAME'] = substr(trim($product->title, '#'), 0, 255);
  $data['CSITPRODUCTSKU'] = substr(trim($product->model, '#'), 0, 255);
  $data['CSITUNITPRICE'] = number_format($product->price, 2, '.', '');
  $data['CSITQUANTITY'] = substr(trim($product->pkg_qty, '#'), 0, 255);
  $data['CSITTOTALAMOUNT'] = substr(trim($amount, '#'), 0, 255);

  $data['CSSTEMAIL'] = $customer_email;
  $data['CSSTCITY'] = 'El Bolsón';
  $data['CSSTCOUNTRY'] = 'AR';
  $data['CSSTFIRSTNAME'] = 'Pepe';
  $data['CSSTLASTNAME'] = 'Sanchez';
  $data['CSSTPHONENUMBER'] = '1123456789';
  $data['CSSTPOSTALCODE'] = '8430';
  $data['CSSTSTATE'] = 'B';
  $data['CSSTSTREET1'] = 'Street';
}

/**
 * Acts on commerce data array being loaded.
 *
 * This hook is invoked during commerce data array loading to send the method sendAuthorizeRequest.
 *
 * @param array $data
 *   An array of commerce data being loaded.
 *
 * @param $order
 *   The ubercart order object.
 */
function hook_uc_todopago_authorization_request_data_commerce($order, &$data) {
  $return_url_error = url('my_todopago/error/' . $order->order_id , array('absolute' => TRUE));
  $data['URL_ERROR'] = $return_url_error;
}
