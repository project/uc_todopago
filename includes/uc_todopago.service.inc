<?php

/**
 * @file
 * Wrapper class for the Todo Pago gateway.
 */

/**
 * Status codes from Todo Pago
 */
define('TODOPAGO_STATUSCODE_APPROVED', -1);
define('TODOPAGO_STATUSCODE_NO_INFORMATION', 0);
define('TODOPAGO_STATUSCODE_REJECTED', 5);
define('TODOPAGO_STATUSCODE_ACCREDITED', 6);
define('TODOPAGO_STATUSCODE_CANCELLED', 7);
define('TODOPAGO_STATUSCODE_REFUNDED', 9);
define('TODOPAGO_STATUSCODE_CONFIRMED_REFUND', 10);
define('TODOPAGO_STATUSCODE_PRE_AUTHORIZED', 11);
define('TODOPAGO_STATUSCODE_OVERDUE', 12);
define('TODOPAGO_STATUSCODE_PENDING_ACCREDITATION', 13);
define('TODOPAGO_STATUSCODE_PENDING_REFUND', 15);
define('TODOPAGO_STATUSCODE_REJECTED_NO_AUTHENTICATION', 24);
define('TODOPAGO_STATUSCODE_REJECTED_INVALID_DATA', 25);
define('TODOPAGO_STATUSCODE_REJECTED_NO_VALIDATED', 32);
define('TODOPAGO_STATUSCODE_TIMEOUT', 38);
define('TODOPAGO_STATUSCODE_INVALID_TRANSACTION', 404);
define('TODOPAGO_STATUSCODE_INVALID_ACCOUNT', 702);
define('TODOPAGO_STATUSCODE_PARAMETRIZATION_ERROR', 720);
define('TODOPAGO_STATUSCODE_SYSTEM_ERROR', 999);
/**
 * Status codes from Ubercart Todo Pago
 */
define('UC_TODOPAGO_PAYMENT_STATUS_PENDING', 15);
define('UC_TODOPAGO_PAYMENT_STATUS_SUCCESS', -1);
define('UC_TODOPAGO_PAYMENT_STATUS_FAILURE', 5);
/**
 * Todo Pago connector using the drupal stored credentials.
 */
class TodoPagoConnector {
  private $settings;
  private $todopago;

  /**
   * Constructor
   */
  public function __construct($settings) {
    if (
      (empty($settings)) ||
      (!is_array($settings)) ||
      (empty($settings['merchant'])) ||
      (empty($settings['authorization'])) ||
      (empty($settings['security'])) ||
      (empty($settings['currency_code'])) ||
      (!in_array($settings['mode'], array('test', 'prod')))
    ) {
      throw new Exception(t('Todo Pago is not configured for use.'));
    }
    $this->settings = $settings;

    $http_header = array(
      'Authorization' => $this->settings['authorization'],
      'user_agent' => 'Commerce_TodoPago',
    );
    //creo instancia de la clase TodoPago
    $this->todopago = new TodoPago\Sdk($http_header, $this->settings['mode']);
  }

  /**
   * Returns an array with the accepted currencies
   *
   * @return
   *   An array with the ISO codes from the accepted currencies
   */
  public static function get_currencies() {
    return array(
      'ARS',
    );
  }

  /**
   * Returns a currency numeric code, as used by Todo Pago
   *
   * @param $currency_code
   *   Currency code to convert
   *
   * @return
   *   The numeric code of the currency
   */
  public static function currency_code($currency_code) {
    if ($currency = commerce_currency_load($currency_code)) {
      return $currency['numeric_code'];
    }
    return FALSE;
  }

  /**
   * Returns the Commerce Status equivalent for a Todo Pago status code
   *
   * @param $status_code
   *   Todo Pago status code
   *
   * @return
   *   Commerce payment status code
   */
  private function payment_status_code($status_code) {
    switch ($status_code) {
      case TODOPAGO_STATUSCODE_INVALID_ACCOUNT:
      case TODOPAGO_STATUSCODE_INVALID_TRANSACTION:
      case TODOPAGO_STATUSCODE_PARAMETRIZATION_ERROR:
        $payment_status = '';
        break;

      case TODOPAGO_STATUSCODE_NO_INFORMATION:
      case TODOPAGO_STATUSCODE_PENDING_ACCREDITATION:
      case TODOPAGO_STATUSCODE_PENDING_REFUND:
      case TODOPAGO_STATUSCODE_PRE_AUTHORIZED:
        $payment_status = UC_TODOPAGO_PAYMENT_STATUS_PENDING;
        break;

      case TODOPAGO_STATUSCODE_ACCREDITED:
      case TODOPAGO_STATUSCODE_APPROVED:
        $payment_status = UC_TODOPAGO_PAYMENT_STATUS_SUCCESS;
        break;

      case TODOPAGO_STATUSCODE_CANCELLED:
      case TODOPAGO_STATUSCODE_CONFIRMED_REFUND:
      case TODOPAGO_STATUSCODE_OVERDUE:
      case TODOPAGO_STATUSCODE_REFUNDED:
      case TODOPAGO_STATUSCODE_REJECTED:
      case TODOPAGO_STATUSCODE_REJECTED_INVALID_DATA:
      case TODOPAGO_STATUSCODE_REJECTED_NO_AUTHENTICATION:
      case TODOPAGO_STATUSCODE_REJECTED_NO_VALIDATED:
      case TODOPAGO_STATUSCODE_SYSTEM_ERROR:
      case TODOPAGO_STATUSCODE_TIMEOUT:
      default:
        $payment_status = UC_TODOPAGO_PAYMENT_STATUS_FAILURE;
        break;

    }

    return $payment_status;
  }

  /**
   * Returns an array with the Drupal form for the redirection to Todo Pago
   *
   * @param $order
   *   Commerce Order
   *
   * @return
   *   An array with the form
   */
  public function create_order_form($order, $data_order, $data_commerce) {
    $form = array();

    $currency_code = $this->settings['currency_code'];
    $order_currency_code = '032';
    $hash = uniqid();

    $operation_id = uc_todopago_remote_id($order, $hash);
    $amount = number_format($order->order_total, 2, '.', '');
    $customer_email = $order->primary_email;
    $return_url_ok = url('uc_todopago/success/' . $order->order_id . '/', array('absolute' => TRUE));
    $return_url_error = url('uc_todopago/error/' . $order->order_id . '/', array('absolute' => TRUE));

    $todopago_info = array(
      'commerce' => array(
        'Security' => $this->settings['security'],
        'EncodingMethod' => 'XML',
        'Merchant' => $this->settings['merchant'],
        'URL_OK' => $return_url_ok,
        'URL_ERROR' => $return_url_error,
      ),
      'order' => array(
        'MERCHANT' => $this->settings['merchant'],
        'OPERATIONID' => $operation_id,
        'CURRENCYCODE' => '032',
        'AMOUNT' => $amount,
        'EMAILCLIENTE' => $customer_email,
        /**
         * Fraud prevention
         */
        'CSBTCOUNTRY' => 'AR',
        'CSBTCUSTOMERID' => $order->uid,
        'CSBTIPADDRESS' => ip_address(),
        'CSBTEMAIL' => $customer_email,
        'CSBTSTATE' => 'B',
        'CSPTCURRENCY' => 'ARS',
        'CSPTGRANDTOTALAMOUNT' => $amount,
      ),
    );

    $todopago_info['order'] = array_merge($todopago_info['order'], $data_order);
    $todopago_info['commerce'] = array_merge($todopago_info['commerce'], $data_commerce);
    $todopago_info['order']['CSBTPHONENUMBER'] = $this->formatPhoneNumber($todopago_info['order']['CSBTPHONENUMBER']);
    $todopago_info['order']['CSSTPHONENUMBER'] = $this->formatPhoneNumber($todopago_info['order']['CSSTPHONENUMBER']);
    //opciones para el método sendAuthorizeRequest
    $authorize_request = $this->todopago->sendAuthorizeRequest($todopago_info['commerce'], $todopago_info['order']);
    if (
      (empty($authorize_request)) ||
      (empty($authorize_request['StatusCode'])) ||
      (empty($authorize_request['URL_Request'])) ||
      (empty($authorize_request['RequestKey'])) ||
      ($authorize_request['StatusCode'] != TODOPAGO_STATUSCODE_APPROVED)
    ) {
      throw new Exception(t('Could not get authorize request: !response', array('!response' => print_r($authorize_request, TRUE))));
    }

    if (!isset($_SESSION['request_key'])) {
       $_SESSION['request_key'] = array();
    }
    $_SESSION['request_key'][$order->order_id] = $authorize_request['RequestKey'];
    return $authorize_request['URL_Request'];
  }

  /**
   * Get the authorize answer of a transaction.
   *
   * @param $transaction
   *   The transaction
   *
   * @return array Info extracted from the answer.
   *   This is an associative array with keys:
   *     - payment_status: commerce status matching the todopago status.
   *     - remote_status: todopago status.
   *     - status_message: description of the status.
   *     - answer: the original response from todopago.
   */
  public function get_authorize_answer($order_id) {
    if (empty($_SESSION['request_key'][$order_id]) || empty($_GET['Answer'])) {
      $msg = t('Missing transaction data to call getAuthorizeAnswer.');
      throw new Exception(t('Could not get authorize answer: !response.', array('!response' => $msg)));
    }

    $get_authorize_answer_info = array(
      'Security' => $this->settings['security'],
      'Merchant'  => $this->settings['merchant'],
      'RequestKey' => $_SESSION['request_key'][$order_id],
      'AnswerKey' => $_GET['Answer'],
    );
    $get_authorize_answer = $this->todopago->getAuthorizeAnswer($get_authorize_answer_info);
    if (
      (empty($get_authorize_answer)) ||
      (empty($get_authorize_answer['StatusCode'])) ||
      (!$payment_status = $this->payment_status_code($get_authorize_answer['StatusCode']))
    ) {
      throw new Exception(t('Could not get authorize answer: !response.', array('!response' => print_r($get_authorize_answer, TRUE))));
    }

    $response = array(
      'payment_status' => $payment_status,
      'remote_status' => $get_authorize_answer['StatusCode'],
      'status_message' => $get_authorize_answer['StatusMessage'],
      'answer' => $get_authorize_answer,
    );

    return $response;
  }

  /**
   * Returns the phone number in the format expected by TodoPago.
   *
   * @param $phone_number
   *   The phone number to format
   */
  public function formatPhoneNumber($phone_number) {
    // TodoPago complains if the phone contains non numeric characters.
    return preg_replace('/[^0-9]/', '', $phone_number);
  }

  /**
   * Removes unwanted characters from string before sending to TodoPago.
   *
   * TodoPago expects multiple values joined by '#', so we cant have that in the
   * texts we send.
   *
   * @param $text
   *   The text to sanitize.
   */
  protected function sanitize($text) {
    return str_replace('#', '', $text);
  }
}

