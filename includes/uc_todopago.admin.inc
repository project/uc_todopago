<?php

/**
 * Returns the form for configuring Todo Pago in the payment methods settings.
 */
function uc_todopago_settings_form() {
  $form = array();

  $form['settings'] = array(
    '#type' => 'vertical_tabs',
  );

// Merge default settings into the stored settings array.
  $settings = uc_todopago_default_settings();

  $form['uc_todopago_merchant'] = array(
    '#type' => 'textfield',
    '#title' => t('Merchant code'),
    '#description' => t('The code of the company in Todo Pago\'s system.'),
    '#default_value' => variable_get('uc_todopago_merchant', $settings['merchant']),
    '#required' => TRUE,
  );
  $form['uc_todopago_currency_code'] = array(
    '#type' => 'select',
    '#title' => t('Default currency'),
    '#description' => t('Transactions in other currencies will be converted to this currency, so multi-currency sites must be configured to use appropriate conversion rates.'),
    '#options' => uc_todopago_currencies(),
    '#default_value' => variable_get('uc_todopago_currency_code', $settings['currency_code']),
  );
  $form['uc_todopago_allow_supported_currencies'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow transactions to use any currency in the options list above.'),
    '#description' => t('Transactions in unsupported currencies will still be converted into the default currency.'),
    '#default_value' => variable_get('uc_todopago_allow_supported_currencies', $settings['allow_supported_currencies']),
  );
  $form['uc_todopago_authorization'] = array(
    '#type' => 'textfield',
    '#title' => t('Authorization code'),
    '#description' => t('The authorization code for the company in Todo Pago\'s system.'),
    '#default_value' => variable_get('uc_todopago_authorization', $settings['authorization']),
    '#required' => TRUE,
  );
  $form['uc_todopago_security'] = array(
    '#type' => 'textfield',
    '#title' => t('Security code'),
    '#description' => t('The security code for the company in Todo Pago\'s system.'),
    '#default_value' => variable_get('uc_todopago_security', $settings['security']),
    '#required' => TRUE,
  );
  $form['uc_todopago_category'] = array(
    '#type' => 'select',
    '#title' => t('Commerce category'),
    '#description' => t('The category of the company\'s bussiness in Todo Pago\'s system.'),
    '#options' => array(
      'retail' => t('Retail'),
      'travel' => t('Travel'),
      'services' => t('Services'),
      'digital_goods' => t('Digital goods'),
      'ticketing' => t('Ticketing'),
    ),
    '#default_value' => variable_get('uc_todopago_category', $settings['category']),
    '#required' => TRUE,
  );
  $form['uc_todopago_mode'] = array(
    '#type' => 'radios',
    '#title' => t('Mode'),
    '#options' => array(
      'test' => t('Development'),
      'prod' => t('Production'),
    ),
    'test' => array('#description' => t('Use for testing the integration.')),
    'prod' => array('#description' => t('Use for processing real transactions.')),
    '#default_value' => variable_get('uc_todopago_mode', $settings['mode']),
  );

  return system_settings_form($form);
}

/**
 * Returns the default settings for the Todo Pago's payment method.
 */
function uc_todopago_default_settings() {
  $default_currency = 'ARS';

  return array(
    'merchant' => '',
    'currency_code' => in_array($default_currency, array_keys(uc_todopago_currencies())) ? $default_currency : 'ARS',
    'allow_supported_currencies' => FALSE,
    'authorization' => '',
    'security' => '',
    'category' => 'retail',
    'mode' => 'test',
  );
}

/**
 * Returns the settings for the Todo Pago's payment method.
 */
function uc_todopago_load_settings() {
  $settings = uc_todopago_default_settings();
  $settings['merchant'] = variable_get('uc_todopago_merchant', '');
  $settings['currency_code'] = variable_get('uc_todopago_currency_code', 'ARS');
  $settings['allow_supported_currencies'] = variable_get('uc_todopago_allow_supported_currencies', FALSE);
  $settings['authorization'] = variable_get('uc_todopago_authorization', '');
  $settings['security'] = variable_get('uc_todopago_security', '');
  $settings['category'] = variable_get('uc_todopago_category', 'retail');
  $settings['mode'] = variable_get('uc_todopago_mode', 'test');
  return $settings;
}

