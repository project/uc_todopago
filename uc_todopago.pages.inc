<?php

/**
 * @file
 * TodoPago pages.
 */

/**
 * TodoPago error url.
 *
 * This url is called when the payment process in TodoPago site was not
 * successful.
 */
function uc_todopago_error($order) {
  try {
    module_load_include('inc', 'uc_todopago', 'includes/uc_todopago.admin');
    $settings = uc_todopago_load_settings();
    $todopago = new TodoPagoConnector($settings);
    $get_authorize_answer_response = $todopago->get_authorize_answer($order->order_id);
    // If it's a valid response we log the TodoPago error.
    if ($get_authorize_answer_response['payment_status'] == UC_TODOPAGO_PAYMENT_STATUS_FAILURE) {
      $params = array(
        '@code' => $get_authorize_answer_response['payment_status'],
        '@error' => $get_authorize_answer_response['status_message'],
        '@order_id' => $order->order_id,
      );
      uc_order_comment_save($order->order_id, 0, t('TodoPago returned error code: @code (@error) processing order @order_id', $params));
      drupal_set_message(t('There was an error processing your payment: @error', $params), 'error');
      watchdog('uc_todopago', 'TodoPago returned error code: @code (@error) processing order @order_id', $params, WATCHDOG_ERROR);
    }
    // The payment for this order was already processed.
    else {
      watchdog('uc_todopago', 'The TodoPago response for the order @order_id was already processed on @date.', array('@order_id' => $order->order_id, '@date' => $get_authorize_answer_response['Payload']['Answer']['DATETIME']), WATCHDOG_WARNING);
      drupal_set_message(t('It seems there was an error with your payment, please contact support if your payment was successful.'), 'status');
    }
  }
  catch (Exception $e) {
    // Invalid checksum, show a generic error to the user and log the event.
    watchdog('uc_todopago', 'get authorize request fail for order @order_id.', array('@order_id' => $order->order_id), WATCHDOG_ERROR);
    drupal_set_message(t("There was an error and we couldn't process your payment, please contact support."), 'error');
  }
  // TODO: add error redirect option to config.
  drupal_goto(variable_get('uc_todopago_error_redirect', 'cart/checkout/review'));
}

/**
 * TodoPago success url.
 *
 * This url is called when the payment process in TodoPago was successful.
 */
function uc_todopago_success($order_id) {
  $order = uc_order_load($order_id);
  // If the get_authorize_answer is correct process finish the order.  
  try {
    module_load_include('inc', 'uc_todopago', 'includes/uc_todopago.admin');
    $settings = uc_todopago_load_settings();
    $todopago = new TodoPagoConnector($settings);
    $get_authorize_answer_response = $todopago->get_authorize_answer($order->order_id);

    uc_cart_empty(uc_cart_get_id());
    $comment = t('Order @id paid in TodoPago with @method', array('@id' => $order->order_id, '@method' => $get_authorize_answer_response['answer']['Payload']['Answer']['PAYMENTMETHODNAME']));
    $_SESSION['uc_checkout'][$_SESSION['cart_order']]['rcc_payment_method'] = $get_authorize_answer_response['answer']['Payload']['Answer']['PAYMENTMETHODNAME'];
    $_SESSION['uc_checkout'][$_SESSION['cart_order']]['rcc_installments'] = $get_authorize_answer_response['answer']['Payload']['Answer']['INSTALLMENTPAYMENTS'];
    if ($get_authorize_answer_response['payment_status'] == UC_TODOPAGO_PAYMENT_STATUS_SUCCESS) {
      uc_payment_enter($order->order_id, 'todopago', $order->order_total, 0, NULL, $comment);
      uc_cart_complete_sale($order);
      uc_order_comment_save($order->order_id, 0, $comment, 'order', 'payment_received');
    } 
    else {
      watchdog('uc_todopago', 'The TodoPago response for the order @order_id was already processed on @date. Status Code: @status', array('@order_id' => $order->order_id, '@date' => $get_authorize_answer_response['Payload']['Answer']['DATETIME'], '@status' => $get_authorize_answer_response['payment_status']), WATCHDOG_WARNING);
      drupal_set_message(t("Your payment is already being processed, please contact us to confirm the order if you don't receive our confirmation email."), 'status');
    }

    // This lets us know it's a legitimate access of the complete page.
    $_SESSION['uc_checkout'][$_SESSION['cart_order']]['do_complete'] = TRUE;
    drupal_goto('cart/checkout/complete');
  }
  catch (Exception $e) {
    watchdog('uc_todopago', 'get authorization answer fail. Order:@order_id, Message: @msg', array('@order_id' => $order->order_id, '@msg' => $e->getMessage()), WATCHDOG_ERROR);
    drupal_set_message(t("There was an error and we couldn't process your payment, please contact support."), 'error');
    drupal_goto(variable_get('uc_todopago_error_redirect', 'cart/checkout/review'));
  }
}
