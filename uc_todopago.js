(function($){
  $.fn.ucTodoPagoRedirect = function(data) {
    $('#uc-todopago-form').hide();
    if (data['submit']) {
      window.location = data['url_todopago'];
    }
    else {
      window.location = data['redirect'];
    }
  }
})(jQuery); 
